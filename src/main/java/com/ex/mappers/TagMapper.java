package com.ex.mappers;

import com.ex.dto.TagDto;
import com.ex.entity.Tag;

import java.util.List;
import java.util.stream.Collectors;

public class TagMapper {
    public static TagDto entity2dto(Tag entity) {
        TagDto dto = new TagDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        return dto;
    }

    public static Tag dto2entity(TagDto dto) {
        Tag entity = new Tag();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        return entity;
    }

    public static List<TagDto> entities2dtoList(List<Tag> entities) {
        return entities.stream().map(TagMapper::entity2dto).collect(Collectors.toList());
    }

    public static List<Tag> dtoList2entities(List<TagDto> dtoList) {
        return dtoList.stream().map(TagMapper::dto2entity).collect(Collectors.toList());
    }
}
