package com.ex.mappers;

import com.ex.dto.UserDto;
import com.ex.entity.User;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapper {

    public static UserDto entity2dto(User entity) {
        UserDto dto = new UserDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        return dto;
    }
    public static User dto2entity(UserDto dto) {
        User entity = new User();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        return entity;
    }

    public static List<UserDto> entities2dtoList(List<User> entities) {
        return entities.stream().map(UserMapper::entity2dto).collect(Collectors.toList());
    }

    public static List<User> dtoList2entities(List<UserDto> dtoList) {
        return dtoList.stream().map(UserMapper::dto2entity).collect(Collectors.toList());
    }
}
