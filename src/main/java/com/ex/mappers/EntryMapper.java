package com.ex.mappers;

import com.ex.dto.EntryDto;
import com.ex.entity.Entry;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class EntryMapper {

    public static EntryDto entity2dto(Entry entity) {
        EntryDto dto = new EntryDto();
        dto.setId(entity.getId());
        dto.setHead(entity.getHead());
        dto.setBody(entity.getBody());
        if (!(entity.getTags() == null)){
            dto.setTagDtoList(TagMapper.entities2dtoList(entity.getTags()));
        }
        return dto;
    }

    public static Entry dto2entity(EntryDto dto) {
        Entry entity = new Entry();
        entity.setId(dto.getId());
        entity.setHead(dto.getHead());
        entity.setBody(dto.getBody());
        if (!(dto.getTagDtoList() == null)) {
            entity.setTags(TagMapper.dtoList2entities(dto.getTagDtoList()));
        }
        return entity;
    }

    public static List<EntryDto> entities2dtoList(List<Entry> entities) {
        return entities.stream().map(EntryMapper::entity2dto).collect(Collectors.toList());
    }

    public static List<Entry> dtoList2entities(List<EntryDto> dtoList) {
        return dtoList.stream().map(EntryMapper::dto2entity).collect(Collectors.toList());
    }
}