package com.ex.service;

import com.ex.dto.EntryDto;
import com.ex.entity.Entry;

import java.util.List;

public interface EntryService {

    Entry create(EntryDto entryDto);

    Entry update(EntryDto entryDto);

    void delete(long id);

    Entry get(long id);

    List<Entry> getAllEntry();
}
