package com.ex.service;

import com.ex.dto.UserDto;
import com.ex.entity.User;

import java.util.List;

public interface UserService {

    User createUser(UserDto userDto);


    User updateUser(UserDto userDto);

    void deleteUser(long id);

    User getUser(long id);

    List<User> getAllUsers();
}
