package com.ex.service.impl;

import com.ex.dto.TagDto;
import com.ex.entity.Tag;
import com.ex.mappers.TagMapper;
import com.ex.repo.TagRepository;
import com.ex.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class TagServiceImpl implements TagService{

    private final TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public Tag create(TagDto tagDto) {
        Tag toSave = TagMapper.dto2entity(tagDto);
        return tagRepository.save(toSave);
    }

    @Override
    public void delete(long id) {
        tagRepository.deleteById(id);
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.findAll();
    }

    public Tag get(long id) {
        Optional<Tag> tag = tagRepository.findById(id);
        if (tag.isEmpty()) {
            throw new NoSuchElementException();
        }
        return tag.get();
    }

    public boolean existsById(long id) {
        return tagRepository.existsById(id);
    }
}
