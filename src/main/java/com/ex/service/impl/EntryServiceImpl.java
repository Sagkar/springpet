package com.ex.service.impl;

import com.ex.dto.EntryDto;
import com.ex.mappers.EntryMapper;
import com.ex.repo.EntryRepository;
import com.ex.entity.Entry;
import com.ex.service.EntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class EntryServiceImpl implements EntryService {
    private final EntryRepository entryRepository;

    @Autowired
    public EntryServiceImpl(EntryRepository entryRepository) {
        this.entryRepository = entryRepository;
    }

    @Override
    public Entry create(EntryDto entryDto) {
        Entry entry = EntryMapper.dto2entity(entryDto);
        return entryRepository.save(entry);
    }

    @Override
    public Entry update(EntryDto entryDto) {
        Optional<Entry> toUpdate = entryRepository.findById((entryDto.getId()));
        if (toUpdate.isEmpty()) {
            throw new NoSuchElementException();
        }
        Entry updated = toUpdate.get();
        updated.setHead(entryDto.getHead());
        updated.setBody(entryDto.getBody());
        return entryRepository.save(updated);
    }

    @Override
    public void delete(long id) {
        entryRepository.deleteById(id);
    }

    @Override
    public Entry get(long id) {
        Optional<Entry> entry= entryRepository.findById(id);
        if (entry.isEmpty()) {
            throw new NoSuchElementException();
        }
        return entry.get();
    }

    @Override
    public List<Entry> getAllEntry() {
        return entryRepository.findAll();
    }

    public boolean existById(long id) {
        return entryRepository.existsById(id);
    }
}
