package com.ex.service.impl;

import com.ex.dto.UserDto;
import com.ex.entity.User;
import com.ex.mappers.UserMapper;
import com.ex.repo.UserRepository;
import com.ex.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User createUser(UserDto userDto) {
        User entity = UserMapper.dto2entity(userDto);
        return userRepository.save(entity);
    }

    @Override
    public User updateUser(UserDto userDto) {
        User newData = UserMapper.dto2entity(userDto);
        Optional<User> toUpdate = userRepository.findById(newData.getId());
        if (toUpdate.isEmpty()) {
            throw new NoSuchElementException();
        }
        toUpdate.get().setName(newData.getName());
        User updated = toUpdate.get();
        return userRepository.save(updated);
    }

    @Override
    public void deleteUser(long id) {
        userRepository.deleteById(id);
    }

    @Override
    public User getUser(long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            throw new NoSuchElementException();
        }
        return user.get();
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public boolean existsById(long id) {
        return userRepository.existsById(id);
    }
}