package com.ex.service;

import com.ex.dto.TagDto;
import com.ex.entity.Tag;

import java.util.List;

public interface TagService{

    Tag create(TagDto tagDto);

    void delete(long id);

    List<Tag> getAll();
}
