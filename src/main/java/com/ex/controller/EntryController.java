package com.ex.controller;

import com.ex.dto.EntryDto;
import com.ex.entity.Entry;
import com.ex.mappers.EntryMapper;
import com.ex.service.impl.EntryServiceImpl;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@RestController
public class EntryController {

    EntryServiceImpl entryService;
    private EntryController(EntryServiceImpl entryService) {
        this.entryService = entryService;
    }
    @RequestMapping(value = "/entry/getAll", method = RequestMethod.GET)
    public List<EntryDto> getAll() {
        List<Entry> entries = entryService.getAllEntry();
        return entries.stream()
                .map(EntryMapper::entity2dto)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/entry/delete", method = RequestMethod.DELETE)
    public void delete(@RequestBody int id) {
        if (!entryService.existById(id)) {
            throw new NoSuchElementException();
        } entryService.delete(id);
    }

    @RequestMapping(value = "/entry/create", method = RequestMethod.POST)
    public EntryDto create(@RequestBody EntryDto entryDto) {
        return EntryMapper.entity2dto(entryService.create(entryDto));
    }

    @RequestMapping(value = "/entry/update", method = RequestMethod.PUT)
    public EntryDto update(@RequestBody EntryDto entryDto) {
        return EntryMapper.entity2dto(entryService.update(entryDto));
    }
}
