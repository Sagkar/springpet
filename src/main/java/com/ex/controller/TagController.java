package com.ex.controller;

import com.ex.dto.TagDto;
import com.ex.entity.Tag;
import com.ex.mappers.TagMapper;
import com.ex.service.impl.TagServiceImpl;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@RestController
public class TagController {

    TagServiceImpl tagService;

    private TagController(TagServiceImpl tagService) {
        this.tagService = tagService;
    }

    @RequestMapping(value = "/tag/create", method = RequestMethod.POST)
    public TagDto create(@RequestBody TagDto tagDto) {
        return TagMapper.entity2dto(tagService.create(tagDto));
    }

    @RequestMapping(value = "/tag/delete", method = RequestMethod.DELETE)
    public void delete(@RequestBody long id) {
        if (!tagService.existsById(id)) {
            throw new NoSuchElementException();
        }
        tagService.delete(id);
    }

    @RequestMapping(value = "/tag/getAll", method = RequestMethod.GET)
    public List<TagDto> getAllTags() {
        List<Tag> tags = tagService.getAll();
        return tags.stream()
                .map(TagMapper::entity2dto)
                .collect(Collectors.toList());
    }
}
