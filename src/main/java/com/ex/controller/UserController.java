package com.ex.controller;

import com.ex.dto.UserDto;
import com.ex.entity.User;
import com.ex.mappers.UserMapper;
import com.ex.service.impl.EntryServiceImpl;
import com.ex.service.impl.UserServiceImpl;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@RestController
public class UserController {

    UserServiceImpl userServiceImpl;
    EntryServiceImpl entryServiceImpl;
    private UserController(UserServiceImpl userServiceImpl, EntryServiceImpl entryServiceImpl) {
        this.userServiceImpl = userServiceImpl;
        this.entryServiceImpl = entryServiceImpl;
    }
    @RequestMapping(value = "/user/getAll", method = RequestMethod.GET)
    public List<UserDto> getAll() {
        List<User> users = userServiceImpl.getAllUsers();
        return users.stream()
                .map(UserMapper::entity2dto)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/user/delete", method = RequestMethod.DELETE)
    public void delete(@RequestBody int id) {
        if (!userServiceImpl.existsById(id)) {
            throw new NoSuchElementException();
        }
        userServiceImpl.deleteUser(id);
    }

    @RequestMapping(value = "/user/create", method = RequestMethod.POST)
    public UserDto create(@RequestBody UserDto userDto) {
        User user = userServiceImpl.createUser(userDto);
        return UserMapper.entity2dto(user);
    }

    @RequestMapping(value = "/user/update", method = RequestMethod.PUT)
    public UserDto update(@RequestBody UserDto userDto) {
        return UserMapper.entity2dto(userServiceImpl.updateUser(userDto));
    }
}
