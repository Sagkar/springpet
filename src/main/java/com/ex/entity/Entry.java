package com.ex.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Entry {

    @Id
    @GeneratedValue
    private long id;

    private String head;

    @NotEmpty(message = "Entry's body cannot be empty")
    private String body;
    @NotNull(message = "Creation date cannot be empty")
    private LocalDateTime creationDate = LocalDateTime.now();

    @ManyToOne
    User user;

    @ManyToMany
    List<Tag> tags;
}
