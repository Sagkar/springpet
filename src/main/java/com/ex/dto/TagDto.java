package com.ex.dto;

import jakarta.persistence.ManyToMany;
import lombok.Data;

import java.util.List;

@Data
public class TagDto {
    private Long id;

    private String name;

    private String description;

    @ManyToMany
    List<EntryDto> entries;
}
