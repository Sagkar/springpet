package com.ex.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EntryDto {
    private long id;

    private String head;

    private String body;

    private LocalDateTime creationDate;

    private long userId;

    List<TagDto> tagDtoList;
}
