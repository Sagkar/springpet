package com.ex.service;

import com.ex.dto.EntryDto;
import com.ex.repo.EntryRepository;
import com.ex.entity.Entry;
import com.ex.entity.User;
import com.ex.service.impl.EntryServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
class EntryServiceImplTest {
    @Mock
    EntryRepository entryRepository;
    @InjectMocks
    EntryServiceImpl entryServiceImpl;

    @Test
    void create() {
        Entry savedEntry = new Entry(1, "TestHead", "TestBody",LocalDateTime.now(),null, null);
        when(entryRepository.save(any(Entry.class))).thenReturn(savedEntry);
        EntryDto entryDto = new EntryDto(1,"UpdatedHeda", "UpdatedBody",LocalDateTime.now(),0, null);
        Entry result = entryServiceImpl.create(entryDto);
        assertEquals(savedEntry, result);
    }

    @Test
    void update() {
        Entry savedEntry = new Entry(1, "TestHead", "TestBody",LocalDateTime.now(), null, null);
        when(entryRepository.findById(1L)).thenReturn(Optional.of(savedEntry));

        Entry updatedEntry = new Entry(1, "UpdatedHead", "UpdatedBody",LocalDateTime.MIN, null, null);
        when(entryRepository.save(any(Entry.class))).thenReturn(updatedEntry);
        EntryDto entryDto = new EntryDto(1, "UpdatedHead", "UpdatedBody",LocalDateTime.MIN, 0, null);
        Entry result = entryServiceImpl.update(entryDto);
        assertEquals(updatedEntry, result);
    }

    @Test
    void delete() {
        entryServiceImpl.delete(1);
        when(entryRepository.findById(1L)).thenReturn(Optional.empty());
        try {
            entryServiceImpl.get(1);
            assert false : "Exception expected";
        } catch (NoSuchElementException e) {
            System.out.println("Entry deleted");
        }
    }

    @Test
    void get() {
        Entry entry = new Entry(1, "TestHead", "TestBody", LocalDateTime.now(), new User(1, "TestName"), null);
        when(entryRepository.findById(1L)).thenReturn(Optional.of(entry));
        Entry result = entryServiceImpl.get(1);
        assertEquals(entry, result);
    }

    @Test
    void getAllEntry() {
            List <Entry> entries = List.of(
                    new Entry(1, "FirstHead", "FirstBody", LocalDateTime.now(), new User(1, "First"), null),
                    new Entry(2, "SecondHead", "SecondBody", LocalDateTime.now(), new User(1, "Second"), null));
            when(entryRepository.findAll()).thenReturn(entries);
            List <Entry> result = entryServiceImpl.getAllEntry();
            assertEquals(entries, result);
    }
}