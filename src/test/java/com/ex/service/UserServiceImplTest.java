package com.ex.service;

import com.ex.dto.UserDto;
import com.ex.entity.User;
import com.ex.repo.UserRepository;
import com.ex.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userServiceImpl;

    @Test
    void createUser() {
        User savedUser = new User();
        savedUser.setId(1);
        savedUser.setName("TestName");
        when(userRepository.save(any(User.class))).thenReturn(savedUser);
        UserDto userDto = new UserDto(1, "TestName");
        User result = userServiceImpl.createUser(userDto);
        assertEquals(savedUser, result);
    }

    @Test
    void updateUser() {
        User savedUser = new User();
        savedUser.setId(1);
        savedUser.setName("TestName");
        when(userRepository.findById(1L)).thenReturn(Optional.of(savedUser));

        User updatedUser = new User();
        updatedUser.setId(1);
        updatedUser.setName("UpdateName");
        when(userRepository.save(any(User.class))).thenReturn(updatedUser);
        UserDto userDto2 = new UserDto(1, "UpdateName");
        User result = userServiceImpl.updateUser(userDto2);
        assertEquals(updatedUser, result);
    }

    @Test
    void deleteUser() {
        userServiceImpl.deleteUser(1);
        when(userRepository.findById(1L)).thenReturn(Optional.empty());
        try {
            userServiceImpl.getUser(1);
            assert false : "Exception expected";
        } catch (NoSuchElementException e) {
            System.out.println("User deleted");
        }
    }

    @Test
    void getUser() {
        User user = new User(1, "TestName");
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        User result = userServiceImpl.getUser(1);
        assertEquals(user, result);
    }

    @Test
    void getAllUsers() {
        List <User> users = List.of(
                new User(1, "First"),
                new User(2, "Second"));
        when(userRepository.findAll()).thenReturn(users);
        List <User> result = userServiceImpl.getAllUsers();
        assertEquals(users, result);
    }
}